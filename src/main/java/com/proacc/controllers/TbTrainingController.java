package com.proacc.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.service.TbTrainingService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TbTrainingController {
	private static final Logger logger = LoggerFactory.getLogger(TbTrainingController.class);
	@Autowired
	TbTrainingService tbTrainingService;
	
	@PostMapping("/getBaru")
	public String getBaru(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<List<Object[]>> tbTraining = tbTrainingService.getallbaru(dataRequest.getString("customer"), dataRequest.getString("company"), dataRequest.getString("periode1"), dataRequest.getString("periode2"), dataRequest.getString("periode3"), dataRequest.getString("periode4"), dataRequest.getString("periode5"));
			tbTraining.get(1).stream().forEach(column ->{
				JSONObject data = new JSONObject();
				data.put("coa", column[0] == null ? "" : column[0].toString());
				data.put("description", column[1] == null ? "" : column[1].toString());
				data.put(tbTraining.get(0).get(0), column[2] == null ? "" : String.format("%.0f", column[2]));
					if(column.length > 4)
					{
					data.put(tbTraining.get(0).get(1), column[3] == null ? "" : String.format("%.0f", column[3]));
					}
					if(column.length > 5)
					{
						data.put(tbTraining.get(0).get(2), column[4] == null ? "" : String.format("%.0f", column[4]));	
					}
					if(column.length > 6)
					{
						data.put(tbTraining.get(0).get(3), column[5] == null ? "" : String.format("%.0f", column[5]));	
					}
					if(column.length > 7)
					{
						data.put(tbTraining.get(0).get(4), column[6] == null ? "" : String.format("%.0f", column[6]));	
					}
				
				data.put("parent", column[column.length - 1] == null ? "" : column[column.length - 1].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data baru berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data baru gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();	
		
	}
}
