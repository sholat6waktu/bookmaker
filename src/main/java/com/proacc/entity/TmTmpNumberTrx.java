package com.proacc.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.proacc.serializable.TmTmpNumberSerializable;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_tmp_number_trx")
@IdClass(TmTmpNumberSerializable.class)
@NamedQueries(  
	    {  
	    	@NamedQuery(name="findByTrxNameAndCompanyId", query = "FROM TmTmpNumberTrx e WHERE e.trxName=?1 and e.companyId = ?2")
	    }
)
public class TmTmpNumberTrx {
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "trx_name")
	private String trxName;
	
	@Column(name = "last_number_trx")
	private int lastNumberTrx;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "created_at")
	
	@CreationTimestamp
	private LocalDateTime createdAt;
	
	@Id
	@Column(name = "company_id")
	private int companyId;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTrxName() {
		return trxName;
	}

	public void setTrxName(String trxName) {
		this.trxName = trxName;
	}

	public int getLastNumberTrx() {
		return lastNumberTrx;
	}

	public void setLastNumberTrx(int lastNumberTrx) {
		this.lastNumberTrx = lastNumberTrx;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	
}
