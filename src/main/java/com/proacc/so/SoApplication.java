package com.proacc.so;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
/* add by Intan Iryanti 23 June 2020*/
@ComponentScan(basePackages = {
		"com.proacc.controllers,"
		+ "com.proacc.entity,"
		+ "com.proacc.repository,"
		+ "com.proacc.service,"
		+ "com.proacc.serviceimpl,"
		+ "com.proacc.config,"
		+ "com.proacc.helper" })
@EnableJpaRepositories("com.proacc.repository")
@EntityScan("com.proacc.entity")
public class SoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoApplication.class, args);
	}

}
