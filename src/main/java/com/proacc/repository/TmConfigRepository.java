package com.proacc.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


import com.proacc.entity.TmConfig;
import com.proacc.serializable.TMConfigSerializable;

public interface TmConfigRepository extends JpaRepository<TmConfig, TMConfigSerializable> {
	public Optional<TmConfig> findByIdMenuAndCompanyCode(final int idMenu, final int companyCode);
//	public Optional<TmTmpNumberTrx> findByCompanyId(final int companyId);
	
}
