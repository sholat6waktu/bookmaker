package com.proacc.helper;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.proacc.service.TmTmpNumberTrxService;
import com.proacc.service.TmConfigService;
import com.proacc.entity.TmConfig;
import com.proacc.entity.TmTmpNumberTrx;

import java.util.Optional;
@RestController
public class Helper {
	
	@Autowired
	TmTmpNumberTrxService TmTmpNumberTrxService;
	
	@Autowired
	TmConfigService TmConfigService;
	
	public static String getUserId(String header )
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
	
	public String GenerateNoTrx(int idFitur, int company)
	{
		String noTrx = "";
		
    	Optional<TmConfig> listConfig = TmConfigService.findByIdMenuAndCompanyCode(idFitur, company);
    	String separator = "";
    	String prefixName = "";
    	String trxName = "";
    	if (listConfig.isPresent()) 
    	{
    		separator = listConfig.get().getSeparator();
    		prefixName = listConfig.get().getPrefixName();
    		trxName = listConfig.get().getPrefixName();
    	}
    	Optional<TmTmpNumberTrx> listTmpNumberTrsx = TmTmpNumberTrxService.getTmpNumberTrx(prefixName, company);

    	int last_number_trx = 0;
    	int last = 0;
    	int config_length = 6;
    	if(listTmpNumberTrsx.isPresent())
    	{
    		last_number_trx = listTmpNumberTrsx.get().getLastNumberTrx();
    		last = last_number_trx + 1;
    	}
    	String lastnumber = null;
    	int angkaLastNumber=0;
    	lastnumber =   String.format("%06d", last);

    	noTrx =prefixName + separator + lastnumber;

    	LocalDateTime createAt = LocalDateTime.now();
		
    	JSONObject data = new JSONObject();
		if(listTmpNumberTrsx.isPresent() && listConfig.isPresent())
		{
			data.put("response", "00");
			data.put("noTrx", noTrx);
			data.put("last", lastnumber);
		}
		else
		{
			data.put("response", "99");
			data.put("noTrx", noTrx);
			data.put("last", lastnumber);
		}
		
		return data.toString();
	}
	
	public String updateNoTrx(int idFitur, int company, int last)
	{
		JSONObject response = new JSONObject();
		try {
			Optional<TmTmpNumberTrx> listUpdate = TmTmpNumberTrxService.findId(idFitur, company);
			
			if(listUpdate.isPresent())	
			{
				((TmTmpNumberTrx)listUpdate.get()).setLastNumberTrx(last);
				TmTmpNumberTrxService.save(listUpdate.get());
			}
			response.put("responseId", "00");
		}
		catch(Exception e)
		{
			response.put("responseId", "99");
			response.put("responseError", e.getMessage());
		}
		
		return response.toString();
	}
}
