package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MIndexSerializable implements Serializable {

	@Column(name = "index_id")
	private Integer indexId;
	
	@Column(name = "m_sotype_id")
	private Integer mSotypeId;
	
	@Column(name = "index_company_id")
	private Integer indexCompanyId;
	
	public MIndexSerializable() {}
	
	public MIndexSerializable(Integer indexId, Integer mSotypeId, Integer indexCompanyId) 
	{
		this.indexId = indexId;
		this.mSotypeId = mSotypeId;
		this.indexCompanyId = indexCompanyId;
	}

	public Integer getIndexId() {
		return indexId;
	}

	public void setIndexId(Integer indexId) {
		this.indexId = indexId;
	}

	public Integer getmSotypeId() {
		return mSotypeId;
	}

	public void setmSotypeId(Integer mSotypeId) {
		this.mSotypeId = mSotypeId;
	}

	public Integer getIndexCompanyId() {
		return indexCompanyId;
	}

	public void setIndexCompanyId(Integer indexCompanyId) {
		this.indexCompanyId = indexCompanyId;
	}
}
