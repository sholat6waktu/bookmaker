package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class TrxSoStrukturSerializable implements Serializable{

	@Column(name = "trx_so_struktur_id")
	private Integer trxSoStrukturId;
	
	@Column(name = "trx_so_level_id")
	private Integer trxSolevelId;
	
	@Column(name = "trx_so_id")
	private Integer trxSoId;
	
	@Column(name = "trx_so_struktur_company_id")
	private Integer trxSostrukturCompanyId;
	
	public TrxSoStrukturSerializable() {}
	
	public TrxSoStrukturSerializable(Integer trxSoStrukturId, Integer trxSoLevelId, Integer trxSoId, Integer trxSoStrukturCompanyId) 
	{
		this.trxSoStrukturId = trxSoStrukturId;
		this.trxSolevelId = trxSoLevelId;
		this.trxSoId = trxSoId;
		this.trxSostrukturCompanyId = trxSoStrukturCompanyId;
	}

	public Integer getTrxSoStrukturId() {
		return trxSoStrukturId;
	}

	public void setTrxSoStrukturId(Integer trxSoStrukturId) {
		this.trxSoStrukturId = trxSoStrukturId;
	}

	public Integer getTrxSolevelId() {
		return trxSolevelId;
	}

	public void setTrxSolevelId(Integer trxSolevelId) {
		this.trxSolevelId = trxSolevelId;
	}

	public Integer getTrxSoId() {
		return trxSoId;
	}

	public void setTrxSoId(Integer trxSoId) {
		this.trxSoId = trxSoId;
	}

	public Integer getTrxSostrukturCompanyId() {
		return trxSostrukturCompanyId;
	}

	public void setTrxSostrukturCompanyId(Integer trxSostrukturCompanyId) {
		this.trxSostrukturCompanyId = trxSostrukturCompanyId;
	}
}
