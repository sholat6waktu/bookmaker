package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxSoLevelSerializable implements Serializable{

	@Column(name = "trx_so_level_id")
	private Integer trxSolevelId;
	
	@Column(name = "trx_so_id")
	private Integer trxSoId;
	
	@Column(name = "trx_so_level_company_id")
	private Integer trxSolevelCompanyId;
	
	public TrxSoLevelSerializable() {}
	
	public TrxSoLevelSerializable(Integer trxSoLevelId, Integer trxSoId, Integer trxSoLevelCompanyId) {
		this.trxSolevelId = trxSoLevelId;
		this.trxSoId = trxSoId;
		this.trxSolevelCompanyId = trxSoLevelCompanyId;
	}
	public Integer getTrxSolevelId() {
		return trxSolevelId;
	}
	public void setTrxSolevelId(Integer trxSolevelId) {
		this.trxSolevelId = trxSolevelId;
	}
	public Integer getTrxSoId() {
		return trxSoId;
	}
	public void setTrxSoId(Integer trxSoId) {
		this.trxSoId = trxSoId;
	}
	public Integer getTrxSolevelCompanyId() {
		return trxSolevelCompanyId;
	}
	public void setTrxSolevelCompanyId(Integer trxSolevelCompanyId) {
		this.trxSolevelCompanyId = trxSolevelCompanyId;
	}
}
