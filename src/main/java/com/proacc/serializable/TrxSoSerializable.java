package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxSoSerializable implements Serializable{

	@Column(name = "trx_so_id")
	private Integer trxSoId;
	
	@Column(name = "trx_so_company_id")
	private Integer trxSoCompanyId;
	
	public TrxSoSerializable() {}
	
	public TrxSoSerializable(Integer trxSoId, Integer trxSoCompanyId)
	{
		this.trxSoId = trxSoId;
		this.trxSoCompanyId = trxSoCompanyId;
	}

	public Integer getTrxSoId() {
		return trxSoId;
	}

	public void setTrxSoId(Integer trxSoId) {
		this.trxSoId = trxSoId;
	}

	public Integer getTrxSoCompanyId() {
		return trxSoCompanyId;
	}

	public void setTrxSoCompanyId(Integer trxSoCompanyId) {
		this.trxSoCompanyId = trxSoCompanyId;
	}
	
}
