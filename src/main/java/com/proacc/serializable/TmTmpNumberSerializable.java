package com.proacc.serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TmTmpNumberSerializable implements Serializable  {
	
	@Column(name = "id")
	public Integer id;
	
	@Column(name = "company_id")
	public int companyId;
	
	
	public TmTmpNumberSerializable() {
		
	}
	
	public TmTmpNumberSerializable(int id,Integer companyId)
	{
		this.id = id;
		this.companyId = companyId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	
	

	
	

}
