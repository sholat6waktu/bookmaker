package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MPositionSerializable implements Serializable{

	@Column(name = "position_id")
	private Integer positionId;
	
	@Column(name = "m_sotype_id")
	private Integer mSotypeId;
	
	@Column(name = "position_company_id")
	private Integer positionCompanyId;
	
	public MPositionSerializable() {}
	
	public MPositionSerializable(Integer positionId, Integer mSotypeId, Integer positionCompanyId)
	{
		this.positionId = positionId;
		this.mSotypeId = mSotypeId;
		this.positionCompanyId = positionCompanyId;
	}

	public Integer getPositionId() {
		return positionId;
	}

	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}

	public Integer getmSotypeId() {
		return mSotypeId;
	}

	public void setmSotypeId(Integer mSotypeId) {
		this.mSotypeId = mSotypeId;
	}

	public Integer getPositionCompanyId() {
		return positionCompanyId;
	}

	public void setPositionCompanyId(Integer positionCompanyId) {
		this.positionCompanyId = positionCompanyId;
	}
}
