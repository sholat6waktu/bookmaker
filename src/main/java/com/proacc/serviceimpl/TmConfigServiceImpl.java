package com.proacc.serviceimpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TmConfig;
import com.proacc.repository.TmConfigRepository;
import com.proacc.service.TmConfigService;

@Service
public class TmConfigServiceImpl implements TmConfigService {
	
	@Autowired
	private TmConfigRepository TmConfigRepository;

	@Override
	public Optional<TmConfig> findByIdMenuAndCompanyCode(int idFitur, int company) {
		// TODO Auto-generated method stub
		return TmConfigRepository.findByIdMenuAndCompanyCode(idFitur, company);
//		return null;
	}

}
