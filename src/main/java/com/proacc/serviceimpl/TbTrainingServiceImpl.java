package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.proacc.service.TbTrainingService;

@Service
public class TbTrainingServiceImpl implements TbTrainingService{
	private static final Logger logger = LoggerFactory.getLogger(TbTrainingServiceImpl.class);
	@Autowired
	EntityManager em;
	@Override
	@Modifying
	public List<List<Object[]>> getallbaru(String customer, String company, String periode1, String periode2, String periode3,
			String periode4, String periode5) {
		// TODO Auto-generated method stub
		this.em.createNativeQuery("\r\n"
				+ "   select cast(baru as varchar) from baru('baru', :compid,:periode1,:periode2, :periode3, :periode4, :periode5);\r\n"
				+ " \r\n"
				+ "   "
				+ "   ")
//				.setParameter("customer", customer)
				.setParameter("compid", company).setParameter("periode1", periode1).setParameter("periode2", periode2).setParameter("periode3", periode3).setParameter("periode4", periode4).setParameter("periode5", periode5)
				.getResultList();
		List<Object[]> nama = this.em.createNativeQuery("(SELECT column_name\r\n"
				+ "\r\n"
				+ "FROM INFORMATION_SCHEMA.COLUMNS\r\n"
				+ "\r\n"
				+ "WHERE TABLE_NAME = 'result' and udt_name = 'float8')").getResultList();
		logger.info(nama.toString());
		String dynamic = " ";
		if(nama.size() > 0)
		{
			for (int i = 0; i < nama.size(); i++) {
				  dynamic = dynamic + "SUM(" + '"' + nama.get(i) + '"' + ") AS " + '"' + nama.get(i) + '"' + ", \r\n";
				}
		}
		List<Object[]> hasil =this.em.createNativeQuery("SELECT\r\n"
				+ "	coa,\r\n"
				+ "	description,\r\n"
				+ dynamic
				+ "	parent \r\n"
				+ "FROM\r\n"
				+ "RESULT \r\n"
				+ "GROUP BY\r\n"
				+ "	coa,\r\n"
				+ "	description,\r\n"
				+ "	parent").getResultList();
		logger.info(hasil.toString());
		List<List<Object[]>> hasilakhir = new ArrayList<List<Object[]>>();
		hasilakhir.add(nama);
		hasilakhir.add(hasil);
		return hasilakhir;
	}
}
