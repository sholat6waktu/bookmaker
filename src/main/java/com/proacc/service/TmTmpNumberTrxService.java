package com.proacc.service;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.TmConfig;
import com.proacc.entity.TmTmpNumberTrx;

@Component
public interface TmTmpNumberTrxService {

	Optional<TmTmpNumberTrx> getTmpNumberTrx(String prefixName, int company);

	Optional<TmTmpNumberTrx> findId(int idFitur, int company);

	void save(TmTmpNumberTrx tmTmpNumberTrx);

}
