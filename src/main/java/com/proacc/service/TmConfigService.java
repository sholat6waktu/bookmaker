package com.proacc.service;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.TmConfig;

@Component
public interface TmConfigService {

	Optional<TmConfig> findByIdMenuAndCompanyCode(int idFitur, int company);

}
